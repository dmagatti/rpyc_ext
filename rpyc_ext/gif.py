import pandas as pd
import numpy as np
from influxdb import DataFrameClient


def generate_columns_string(columns):
    column_string = ''
    for ix,column in enumerate(columns):
        if ix == 0:
            column_string = column_string + f'select mean("{column}") as "{column}"'
        else:
            column_string = column_string + f', mean("{column}") as "{column}"'
    return column_string


class RpcReader:
    def __init__(self,*args,**kwargs):
        #Set default parameters
        #time settings
        self.tstart = '2020-08-04'
        self.tstop = '2020-09-26'
        self.time_interval = '1m'
        #influxdb general parameters
        self.hv_module_name = 'hvrpc256new'
        self.host = 'dbod-epdtmon.cern.ch'
        self.port = 8081
        self.dbname = 'data'
        self.user = 'user'
        self.password = 'password'
        self.rpc = 'RPC0'
        self.columns=['VMon','IMon']
        self.query = 'hv'
        #Update provided parameters
        self.__dict__.update(kwargs)
        #client
        self.client = DataFrameClient(self.host, self.port, self.user, self.password, self.dbname, ssl=True, verify_ssl=False)
    
    
    def read_rpc_data(self):
        tstart = pd.to_datetime(self.tstart)
        tstart = tstart.isoformat()
        tstop = pd.to_datetime(self.tstop)
        tstop = tstop.isoformat()
        query_str = generate_columns_string(self.columns)+ f''' FROM "{self.query}" WHERE ("module" = '{self.hv_module_name}' AND "channel_name" = '{self.rpc}') AND time >= '{tstart}Z' AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)'''
        ddict = self.client.query(query_str)
        df = ddict.get(self.query)
        df['rpc'] = self.rpc[-1]
        return df 

    
class EnConReader():
    def __init__(self,*args,**kwargs):
        #Set default parameters
        #Reference pressure, temperature
        self.p0 = 965
        self.t0 = 20 + 273.15
        #GIF conditions
        self.pressure_dip_sub = 'dip/GIFpp/Atmospheric_Pressure'
        self.temperature_dip_sub = 'dip/GIFpp/Temp_Inside_Bunker'
        self.humidity_dip_sub = 'dip/GIFpp/Humidity_Inside_Bunker'
        self.source_attenuation_dip_sub = 'dip/GIFpp/Attenuators/UpStreamPos/EffectiveAttenuation'
        self.source_status_dip_sub = 'dip/GIFpp/Irradiator/SourceON'
        #Default field
        self.field = 'Float_value'
        #time settings
        self.tstart = '2020-08-04'
        self.tstop = '2020-09-26'
        self.time_interval = '1m'
        #influxdb general parameters
        self.host = 'dbod-epdtmon.cern.ch'
        self.port = 8081
        self.dbname = 'data'
        self.user = 'user'
        self.password = 'password'
        self.query = 'dip'
        #Update provided parameters
        self.__dict__.update(kwargs)
        #client
        self.client = DataFrameClient(self.host,self.port,self.user,self.password,self.dbname,ssl=True,verify_ssl=False)

    def read_EnCon(self):
        tstart = pd.to_datetime(self.tstart)
        tstart = tstart.isoformat()
        tstop = pd.to_datetime(self.tstop)
        tstop = tstop.isoformat()
        #pressure
        query_str =  f'''SELECT mean("{self.field}") as bunker_pressure FROM "dip" WHERE ("sub" = '{self.pressure_dip_sub}')
                    AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        df = ddict.get(self.query)
        #temperature
        query_str = f'''SELECT mean("{self.field}") as bunker_temperature FROM "dip" WHERE ("sub" = '{self.temperature_dip_sub}')
                    AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        df = pd.merge(df,ddict.get(self.query),left_index=True,right_index=True)
        #humidity
        query_str = f'''SELECT mean("{self.field}") as bunker_humidity FROM "dip" WHERE ("sub" = '{self.humidity_dip_sub}')
                    AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        df = pd.merge(df,ddict.get(self.query),left_index=True,right_index=True)
        #status
        query_str = f'''SELECT mean("Int_value") as source_status FROM "dip" WHERE ("sub" = '{self.source_status_dip_sub}')
                    AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(linear)''' 
        ddict = self.client.query(query_str)
        df = pd.merge(df,ddict.get(self.query),left_index=True,right_index=True)
        #attenuation
        query_str = f'''SELECT mean("{self.field}") as source_attenuation FROM "dip" WHERE ("sub" = '{self.source_attenuation_dip_sub}')
                    AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(linear)''' 
        ddict = self.client.query(query_str)
        df = pd.merge(df,ddict.get(self.query),left_index=True,right_index=True)        
        return df 
        

class GasStatusReader():
    def __init__(self,*args,**kwargs):
        #Default field
        self.field = 'Float_value'
        #time settings
        self.tstart = '2020-08-04'
        self.tstop = '2020-09-26'
        self.time_interval = '1m'
        #influxdb general parameters
        self.host = 'dbod-epdtmon.cern.ch'
        self.port = 8081
        self.dbname = 'data'
        self.user = 'user'
        self.password = 'password'
        self.query = 'picolog'
        #Update provided parameters
        self.__dict__.update(kwargs)
        #client
        self.client = DataFrameClient(self.host,self.port,self.user,self.password,self.dbname,ssl=True,verify_ssl=False) 
    
    def read_GasStatus(self):
        tstart = pd.to_datetime(self.tstart)
        tstart = tstart.isoformat()
        tstop = pd.to_datetime(self.tstop)
        tstop = tstop.isoformat()
        #fresh input       
        query_str =  f'''SELECT mean("Finput") as Finput FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z'  GROUP BY time(1m) fill(null)''' 
        ddict = self.client.query(query_str)
        df = ddict.get(self.query)
        df['Finput'] = df['Finput']*0.3099
        #input1
        query_str =  f'''SELECT mean("F1") as Input1 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z'  GROUP BY time(1m) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['Input1'] = this_df['Input1']*0.3099
        df = pd.merge(df,this_df,left_index=True,right_index=True)
        #input2
        query_str =  f'''SELECT mean("F2") as Input2 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z'  GROUP BY time(1m) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['Input2'] = this_df['Input2']*0.3099
        df = pd.merge(df,this_df,left_index=True,right_index=True)       
        #input3
        query_str =  f'''SELECT mean("F3") as Input3 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z'  GROUP BY time(1m) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['Input3'] = this_df['Input3']*0.3099
        df = pd.merge(df,this_df,left_index=True,right_index=True)        
        #O2 concentration
        query_str =  f'''SELECT mean("O2") as O2 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z'  GROUP BY time(1m) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['O2'] = this_df['O2']
        df = pd.merge(df,this_df,left_index=True,right_index=True)        
        #recirculation fraction
        df['recirculation_fraction'] = df['Finput']/(df['Input1']+df['Input2']+df['Input3'])
        return df

        tstart = pd.to_datetime(self.tstart)
        tstart = tstart.isoformat()
        tstop = pd.to_datetime(self.tstop)
        tstop = tstop.isoformat()
        #fresh input       
        query_str =  f'''SELECT mean("Finput") as Finput FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        df = ddict.get(self.query)
        df['Finput'] = df['Finput']*0.3099
        #input1
        query_str =  f'''SELECT mean("F1") as Input1 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['Input1'] = this_df['Input1']*0.3099
        df = pd.merge(df,this_df,left_index=True,right_index=True)
        #input2
        query_str =  f'''SELECT mean("F2") as Input2 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['Input2'] = this_df['Input2']*0.3099
        df = pd.merge(df,this_df,left_index=True,right_index=True)       
        #input3
        query_str =  f'''SELECT mean("F3") as Input3 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['Input3'] = this_df['Input3']*0.3099
        df = pd.merge(df,this_df,left_index=True,right_index=True)        
        #O2 concentration
        query_str =  f'''SELECT mean("O2") as O2 FROM "picolog" WHERE ("place" = 'gif')
            AND time >= '{tstart}Z'  AND time <= '{tstop}Z' GROUP BY time({self.time_interval}) fill(null)''' 
        ddict = self.client.query(query_str)
        this_df = ddict.get(self.query)
        this_df['O2'] = this_df['O2']
        df = pd.merge(df,this_df,left_index=True,right_index=True)        
        #recirculation fraction
        df['recirculation_fraction'] = df['Finput']/(df['Input1']+df['Input2']+df['Input3'])
        return df