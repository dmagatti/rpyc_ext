import pandas as pd
import numpy as np

#la default map {rpc,channel} as a dictionary
#un reader per il file
#un qualcosa che mi calcoli (VMon,IMon) medio a partire dai dati letti   
#un influxdb reader per pressione e temperatura per correggere VMon     #molto piu' easy passare due valori uardati a mano da grafana
#Un oggetto che mi ritorni un df organizzato come (data,rpc,Veff,IMon)

default_map = {
  "0": "RPC0",
  "1": "RPC1",
  "2": "RPC2"
}


class scan_analyzer:
    def __init__(self,*args,**kwargs):
        #set default parameters
        self.channel_map = default_map
        self.p0 = 965
        self.t0 = 20 + 273.15
        self.p = 965
        self.t = 20 + 273.15
        self.basepath = '/eos/project/g/ghg-studies/BackupRPC/gif/'
        self.date = '20201118/'
        self.time = '16_00_31'
        self.filepath = self.basepath + self.date + 'scan_' + self.time + '.csv'
        #Update provided parameters
        self.__dict__.update(kwargs)

    def read_rpc_data(self):
        df_scan = pd.read_csv(self.filepath,names=['V','channel','IMon','VMon'])
        df_scan['VEff'] = df.scan.VMon*self.t/self.t0*self.p0/self.p 
        return df 
'




