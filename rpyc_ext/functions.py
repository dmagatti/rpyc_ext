import pandas as pd
import numpy as np
import rpyc.legacy.functions as rf
import scipy as sp
from scipy import odr
from scipy.odr import Model,RealData,ODR

def fit_function(parameters,x):
    '''Fit function for the sigmoid efficiency-streamer probability curvers

    Arguments:
        initial_parameters {[float,float,float]} -- fit parameters: [effmax,gamma,hv50]
    '''
    effmax = parameters[0]
    gamma = parameters[1]
    hv50 = parameters[2]
    return effmax/(1+np.exp(-gamma*(x - hv50)))

###########################################################################################################################################################################################################################################################################################################

def save_lite_output(mixture,df):
    '''Returns run parameters by reading run dataframe
    A separation between avalanche and streamer pulsesu using cumulative charge (sum of fired strip charges) hardcoded and set to 7.5 pC

    Arguments:
        mixture {string} -- string describing the gas mixture
        df {pandas dataframe} -- dataframe, result of analyse_and_save_lite functions defined in rpyc/functions
    '''
    lite_output = pd.DataFrame(columns = ['mixture','rpc','voltage_eff','efficiency','efficiency_error','streamer_probability','streamer_probability_error','type','mean_charge_strip','mean_charge_event','time_resolution','time_resolution_error','cluster_size','cluster_size_error'])

    for rpc,rpc_group in df.groupby('rpc'):
        for v_eff,v_eff_group in rpc_group.groupby('voltage_eff'):
            time_resolution_avalanche = v_eff_group.query('is_detected & type=="avalanche"').fwhm_time.mean()
            time_resolution_error_avalanche = v_eff_group.query('is_detected & type=="avalanche"').fwhm_time.std()
            time_resolution_streamer = v_eff_group.query('is_detected & type=="streamer"').fwhm_time.mean()
            time_resolution_error_streamer = v_eff_group.query('is_detected & type=="streamer"').fwhm_time.std()  

            cluster_size_avalanche = v_eff_group.query('is_detected & type=="avalanche"').cluster_size.mean()
            cluster_size_error_avalanche = v_eff_group.query('is_detected & type=="avalanche"').cluster_size.std()
            cluster_size_streamer = v_eff_group.query('is_detected & type=="streamer"').cluster_size.mean()
            cluster_size_error_streamer = v_eff_group.query('is_detected & type=="streamer"').cluster_size.std()

            mean_charge_avalanche = v_eff_group.query('is_detected & type=="avalanche"').charge.mean()
            mean_charge_streamer = v_eff_group.query('is_detected & type=="streamer"').charge.mean()        
            mean_charge_event_avalanche = v_eff_group.query('is_detected').groupby('nevent').charge.sum()
            mean_charge_event_avalanche = mean_charge_event_avalanche[mean_charge_event_avalanche<7.5].mean()
            mean_charge_event_streamer = v_eff_group.query('is_detected').groupby('nevent').charge.sum()
            mean_charge_event_streamer = mean_charge_event_streamer[mean_charge_event_streamer>=7.5].mean()
            
            efficiency = v_eff_group.efficiency.unique()[0]
            efficiency_error = v_eff_group.efficiency_error.unique()[0]
            streamer_probability = v_eff_group.streamer_probability.unique()[0]
            streamer_probability_error = v_eff_group.streamer_probability_error.unique()[0]

            results_avalanche = [mixture,rpc,v_eff,efficiency,efficiency_error,streamer_probability,streamer_probability_error,'avalanche',mean_charge_avalanche,mean_charge_event_avalanche,time_resolution_avalanche,time_resolution_error_avalanche,cluster_size_avalanche,cluster_size_error_avalanche]
            lite_output = lite_output.append(dict(zip(lite_output.columns, results_avalanche)), ignore_index=True)
            results_streamer = [mixture,rpc,v_eff,efficiency,efficiency_error,streamer_probability,streamer_probability_error,'streamer',mean_charge_streamer,mean_charge_event_streamer,time_resolution_streamer,time_resolution_error_streamer,cluster_size_streamer,cluster_size_error_streamer]
            lite_output = lite_output.append(dict(zip(lite_output.columns, results_streamer)), ignore_index=True)
    return lite_output

###########################################################################################################################################################################################################################################################################################################

def get_values_at_wp(lite_output,hv50,voltage_limits,effmax=0.95,fit_mode='rf'):
    '''
    Given a lite_output-like input, returns a pandas dataframe with parameter values at knee voltage and working point. Working point is defined as knee_voltage+150V
    Only efficiency is fitted (using a fit function defined in rpyc), and parameters value is obrating by using linear interpolation

    Arguments:
        lite_output {pandas dataframe} -- dataframe with run parameters 
        hv50 {int/float} -- HV value corresponding to 50% efficiency 
        voltage_limits {(int,int)} -- lower and upper voltage bounds for hv50 to use while fitting efficiency curve 
        effmax {float} -- maximum reached efficiency reache durin run; default value 0.95
        
    Output:
        [efficiency,efficiency error,streamer probability,streamer probability error,cluster size,time resolution] @knee_voltage
        [efficiency,efficiency error,streamer probability,streamer probability error,cluster size,time resolution] @working_point
    '''
    if voltage_limits == None:
        voltage_limits = (hv50-500,hv50+500)
    knee_params = pd.DataFrame(columns=['rpc','knee[V]','efficiency_knee','efficiency_err_knee','stprob_knee','stprob_knee_err','mean_charge_avalanche_knee[pC]','mean_charge_streamer_knee[pC]','time_resolution_knee[ns]','cluster_size_knee','current_knee'])
    wp_params = pd.DataFrame(columns=['rpc','working_point[V]','efficiency_wp','efficiency_err_wp','stprob_wp','stprob_wp_err','mean_charge_avalanche_wp[pC]','mean_charge_streamer_wp[pC]','time_resolution_wp[ns]','cluster_size_wp','current_wp'])
    for ix, (rpc,rpc_group) in enumerate(lite_output.groupby('rpc')):
        #load from lite output
        voltage_eff = np.array(rpc_group['voltage_eff'].astype(int))
        efficiency = np.array(rpc_group['efficiency'])
        efficiency_err = np.array(rpc_group['efficiency_error'])
        stprob = np.array(rpc_group['streamer_probability'])
        stprob_err = np.array(rpc_group['streamer_probability_error'])
        time_resolution = np.array(rpc_group['time_resolution'])
        cluster_size = np.array(rpc_group['cluster_size'])
        mean_charge_avalanche = np.array(rpc_group.query('type == "avalanche"')['mean_charge_strip'])
        mean_charge_streamer = np.array(rpc_group.query('type == "streamer"')['mean_charge_strip'])    
        current = np.array(rpc_group['current'])
        #fit efficiency
        if fit_mode=='rf':
            effmax, gamma, hv50, *_ = rf.fit_efficiency(voltage_eff, efficiency, efficiency_err,initial_params=[0.95, 0.1, hv50],voltage_limits=voltage_limits)
        elif fit_mode=='odr':
            initial_parameters=[effmax,0.01,hv50]
            mymodel = Model(fit_function)
            mydata = RealData(voltage_eff,efficiency,sy=efficiency_err)
            myodr = ODR(mydata,mymodel,beta0=initial_parameters)
            myoutput=myodr.run()
            fit_parameters = myoutput.beta
            effmax = fit_parameters[0]
            gamma = fit_parameters[1]
            hv50 = fit_parameters[2]    
        knee = rf.get_hv_efficiency_knee(effmax, gamma, hv50)
        wp = knee+150
        #extrapolate parameters @knee
        efficiency_knee = np.interp(knee, voltage_eff, efficiency)
        efficiency_err_knee = np.interp(knee, voltage_eff, efficiency_err)                    
        stprob_knee = np.interp(knee, voltage_eff, stprob)  
        stprob_knee = np.interp(knee, voltage_eff, stprob)
        stprob_err_knee = np.interp(knee, voltage_eff, stprob_err)
        mean_charge_avalanche_knee = np.interp(knee,np.unique(voltage_eff),mean_charge_avalanche)
        mean_charge_streamer_knee = np.interp(knee,np.unique(voltage_eff),mean_charge_streamer)                     
        time_resolution_knee = np.interp(knee, voltage_eff, time_resolution)
        cluster_size_knee = np.interp(knee, voltage_eff, cluster_size)
        current_knee = np.interp(knee, voltage_eff, current)
        rpc_params = [rpc,knee,efficiency_knee,efficiency_err_knee,stprob_knee,stprob_err_knee,mean_charge_avalanche_knee,mean_charge_streamer_knee,time_resolution_knee,cluster_size_knee,current_knee]
        knee_params = knee_params.append(dict(zip(knee_params.columns, rpc_params)), ignore_index=True) 
        #extrapolate parameters @WP
        efficiency_wp = np.interp(wp, voltage_eff, efficiency)
        efficiency_err_wp = np.interp(wp, voltage_eff, efficiency_err)            
        stprob_wp = np.interp(wp, voltage_eff, stprob)
        stprob_err_wp = np.interp(wp, voltage_eff, stprob_err)
        mean_charge_avalanche_wp = np.interp(wp,np.unique(voltage_eff),mean_charge_avalanche)
        mean_charge_streamer_wp = np.interp(wp,np.unique(voltage_eff),mean_charge_streamer)                                                
        time_resolution_wp = np.interp(wp, voltage_eff, time_resolution)
        cluster_size_wp = np.interp(wp, voltage_eff, cluster_size)
        current_wp = np.interp(wp, voltage_eff, current)
        rpc_params = [rpc,wp,efficiency_wp,efficiency_err_wp,stprob_wp,stprob_err_wp,mean_charge_avalanche_wp,mean_charge_streamer_wp,time_resolution_wp,cluster_size_wp,current_wp]
        wp_params = wp_params.append(dict(zip(wp_params.columns, rpc_params)), ignore_index=True) 
    return knee_params,wp_params

###########################################################################################################################################################################################################################################################################################################

