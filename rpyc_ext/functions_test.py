import pandas as pd
import numpy as np
import scipy as sp
from scipy import odr
from scipy.odr import Model,RealData,ODR


def get_HV_error(HV,p,t,delta_p,delta_t):
    '''
    Evaluate error on HV_effective computed values using (pressure,temperature) variation during run; approximate result which works for small delta_p/p

    Arguments:
        HV {pandas series/numpy array} -- HV values of the run (NOT corrected for pressure/temperature)
        p {float} -- pressure value during run [mbar]
        T {float} -- temperature value during run [C]
        deltaP {float} -- pressure variation during run [mbar]
        deltaT {float} -- temperature variation during run [K]
    '''
    t0 = 293.13
    p0 = 965
    return np.abs(-HV*t/t0*p0/p*delta_p/p + HV*delta_t/t0*p0/p*(1-delta_p/p))


def fit_function(parameters,x):
    '''Fit function for the sigmoid efficiency-streamer probability curvers

    Arguments:
        initial_parameters {[float,float,float]} -- fit parameters: [effmax,gamma,hv50]
    '''
    effmax = parameters[0]
    gamma = parameters[1]
    hv50 = parameters[2]
    return effmax/(1+np.exp(-gamma*(x - hv50)))


def fit_efficiency_test(HV,HV_error,efficiency,efficiency_error,initial_parameters=[0.95,0.01,9200],voltage_limits=None):
    '''Efficiency curve fit using HV_error and efficiency_error. Fit method: ODR

    Arguments:
        HV {pandas series/numpy array} -- HV values of the run (altready corrected for pressure and temperature)
        HV_error {pandas series/numpy array} -- error on HV values of the run
        efficiency {pandas series/numpy array} -- efficiency values of the run
        efficiency_error {pandas series/numpy array} -- error on efficiency values of the run
        initial_parameters {[effmax,gamma,hv50]} -- initial parameters for fit 
        voltage_limits {(int,int)} -- lower and upper voltage bounds for hv50 to use while fitting efficiency curve; if "None", (hv50-500,hv500+500) is used as default value

    Output
        [effmax,gamma,hv50],[effmax_error,gamma_error,hv50_error],chi2ndf
    '''
    HV = np.asarray(HV, dtype=float)
    HV_error = np.asarray(HV_error, dtype=float)
    efficiency = np.asarray(efficiency, dtype=float)
    efficiency_error = np.asarray(efficiency_error, dtype=float)
    if voltage_limits == None:
        voltage_limits = (initial_parameters[2]-500,initial_parameters[2]+500)
    mymodel = Model(fit_function)
    mydata = RealData(HV,efficiency,sx=HV_error,sy=efficiency_error)
    myodr = ODR(mydata,mymodel,beta0=initial_parameters)
    myoutput=myodr.run()
    fit_parameters = myoutput.beta
    fit_parameters_error = myoutput.sd_beta
    chi2ndf = myoutput.res_var
    return fit_parameters,fit_parameters_error,chi2ndf


def iteratively_fit_efficiency(HV,HV_error,efficiency,efficiency_error,initial_parameters=[0.95,0.01,9200],voltage_limits=None,max_it=10):
    '''Efficiency curve fit using HV_error and efficiency_error, with efficiency error corrected iteratively. Fit method: ODR

    Arguments:
        HV {pandas series/numpy array} -- HV values of the run (altready corrected for pressure and temperature)
        HV_error {pandas series/numpy array} -- error on HV values of the run
        efficiency {pandas series/numpy array} -- efficiency values of the run
        efficiency_error {pandas series/numpy array} -- error on efficiency values of the run
        initial_parameters {[effmax,gamma,hv50]} -- initial parameters for fit 
        voltage_limits {(int,int)} -- lower and upper voltage bounds for hv50 to use while fitting efficiency curve; if "None", (hv50-500,hv500+500) is used as default value
        max_it {int} -- maximum number of error-correcting iterations

    Output
        [effmax,gamma,hv50],[effmax_error,gamma_error,hv50_error],chi2ndf
    '''
    HV = np.asarray(HV, dtype=float)
    HV_error = np.asarray(HV_error, dtype=float)
    efficiency = np.asarray(efficiency, dtype=float)
    efficiency_error = np.asarray(efficiency_error, dtype=float)
    if voltage_limits == None:
        voltage_limits = (initial_parameters[2]-500,initial_parameters[2]+500)
    mymodel = Model(fit_function)
    mydata = RealData(HV,efficiency,sx=HV_error,sy=efficiency_error)
    myodr = ODR(mydata,mymodel,beta0=initial_parameters)
    myoutput=myodr.run()
    fit_parameters = myoutput.beta
    fit_parameters_error = myoutput.sd_beta
    chi2ndf = myoutput.res_var
    for i in range(max_it):
        new_efficiency_error = efficiency_error/np.sqrt(fit_parameters[0])
        mydata = RealData(HV,efficiency,sx=HV_error,sy=new_efficiency_error)
        myodr = ODR(mydata,mymodel,beta0=initial_parameters)
        myoutput=myodr.run()
        if (np.abs(1-myoutput.res_var)<np.abs(1-chi2ndf)):
            fit_parameters = myoutput.beta
            fit_parameters_error = myoutput.sd_beta
            chi2ndf = myoutput.res_var
        else: 
            break
    return fit_parameters,fit_parameters_error,chi2ndf