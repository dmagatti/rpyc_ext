import pandas as pd
import numpy as np
from itertools import groupby 
from pathlib import Path
import os


#hardcoded defalut parameters - can be changed while invoking functions
adc_to_mv = 0.122
resistance = 56
record_length = 520
noise_threshold = 10 #ADC
height_threshold = 16 #ADC
sample_points_window = 100
timestep = 2 #ns
baseline_interval = [0,250]
channel_map = None


def read_file(filepath,record_length=record_length):
    '''
   Return a numpy array; every item of the array is record_length long and contains data acquired after a scintillator trigger

    Arguments:
        filepath {string} -- path of the wavefile
        record_length {float} -- number of sample points in a recorded signal
    '''
    wave = np.array(pd.read_csv(filepath))
    wave = np.append(wave,wave[-1])
    wave = np.reshape(wave,(-1,record_length))
    return wave


def calc_cumulative_charge(signal,sample_points_window=sample_points_window,timestep=timestep,adc_to_mv=adc_to_mv,resistance=resistance,record_length=record_length):
    '''
    Returns a numpy array containing the cumulative charge; 
    output[n] = total induced charge until position[n] in the signal

    Arguments:
        signal {np.array} -- record_length long array with data recorded after trigger
    '''
    signal_charge = []
    for i in range(sample_points_window):
        signal_charge.append(np.sum(signal[:i]))
    signal_charge = np.array(signal_charge)*adc_to_mv*timestep/resistance
    return signal_charge


def moving_average(a, n=3):
    '''
    A simple smooting function

    Arguments
        a {np.array} -- array to be smoothed
    '''
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def analyze_shape(wave,sample_points_window=sample_points_window,timestep=timestep,adc_to_mv=adc_to_mv,resistance=resistance,
                record_length=record_length,baseline_interval=baseline_interval,noise_threshold=noise_threshold,height_threshold=height_threshold):
    '''
    Returns an array with charge development over time for a single strip

    Arguments:
        wave {pd.Series} -- read_file function output
        timestep {float} -- time step between two different oscilloscope recorded sample points
        adc_to_mv {float} -- adc to mV conversion factor
        resistance {float} -- resistance between strip and ground
        record_length {int} -- number of points in a recorded signal
    '''
    time = np.linspace(0,timestep*sample_points_window,sample_points_window)
    df = pd.DataFrame(columns=['data','data_flat','charge_data','smoothed_charge_data','minimum','n_pulses','widths','cumulative_charge','event'])
    for ix,signal in enumerate(wave):
        signal = signal.astype(float)
        baseline = np.mean(signal[:baseline_interval[1]])
        baseline_std = np.std(signal[:baseline_interval[1]])
        raw_signal = np.copy(signal)
        signal[np.abs(signal-baseline)<noise_threshold] = baseline
        signal = signal - baseline
        minimum = min(signal)
        if max(np.abs(signal[baseline_interval[1]:baseline_interval[1]+sample_points_window]))>height_threshold:
            signal_cumulative_charge = calc_cumulative_charge(signal[baseline_interval[1]:baseline_interval[1]+sample_points_window],timestep=timestep,adc_to_mv=adc_to_mv,sample_points_window=sample_points_window)
            smoothed_charge = np.round(moving_average(signal_cumulative_charge,7),7)
            widths = np.array([sum(val)-5 for keys, val in groupby(np.ediff1d(smoothed_charge)!=0, key = lambda x: x != 0) if keys != 0])
            widths = [x for x in widths if x>=3]
        else:
            signal_cumulative_charge = np.zeros(sample_points_window)
            smoothed_charge = np.zeros(sample_points_window)
            widths = np.array([0])
        row = [raw_signal[baseline_interval[1]:baseline_interval[1]+sample_points_window],signal[baseline_interval[1]:baseline_interval[1]+sample_points_window],signal_cumulative_charge,smoothed_charge,minimum,len(widths),widths,signal_cumulative_charge[-1],ix]
        df = df.append(dict(zip(df.columns,row)), ignore_index=True)
    return df


def analyse_shape_HV(path_HV,waves,sample_points_window=sample_points_window,timestep=timestep,adc_to_mv=adc_to_mv,resistance=resistance,
                record_length=record_length,baseline_interval=baseline_interval,noise_threshold=noise_threshold,height_threshold=height_threshold,
                channel_map=channel_map):
    '''
    Performs analyze_shape on all waves at a given HV
    '''    
    df = pd.DataFrame(columns=['data','data_flat','charge_data','smoothed_charge_data','minimum','n_pulses','widths','cumulative_charge','event','wave'])
    for r,d,f in os.walk(path_HV):
        for file in f:
            if Path(file).stem[0] == 'w':            
                filepath = os.path.join(r, file)
                wave_number = file.replace('wave', '').replace('.txt','')
                if wave_number in waves:
                    print(f'Analyzing {Path(file).stem}')
                    wave_analysis = analyze_shape(read_file(filepath,record_length=record_length),sample_points_window=sample_points_window,timestep=timestep,adc_to_mv=adc_to_mv,resistance=resistance,
                                            record_length=record_length,baseline_interval=baseline_interval,noise_threshold=noise_threshold,
                                            height_threshold=height_threshold)
                    wave_analysis['wave'] = np.full_like(np.array(wave_analysis.index),wave_number)
                    df = df.append(wave_analysis)
    return df


def add_cluster_size(analyzed_hv_df):
    cluster_size_all = []
    analyzed_hv_df = analyzed_hv_df.sort_values(by=['event','wave'])
    for event,event_group in analyzed_hv_df.groupby('event'):
        cluster_size = np.sum(np.array(event_group.widths.ravel())!=0)
        cluster_size_all.append(cluster_size*np.ones(len(event_group)))
    analyzed_hv_df['cluster_size'] = np.array(cluster_size_all).flatten()
    return analyzed_hv_df


def analyse_shape_event_HV(analyzed_hv_df,timestep=timestep,adc_to_mv=adc_to_mv,noise_threshold=noise_threshold,height_threshold=height_threshold,baseline_interval=baseline_interval,sample_points_window=sample_points_window):
    wave_event = analyzed_hv_df.query('wave>0').groupby('event')['data_flat'].apply(lambda x: [sum(y) for y in zip(*x)])
    wave_event = np.array([np.concatenate((np.full(baseline_interval[1],np.mean(x[0:20])),np.array(x))) for x in wave_event]).astype(int)
    wave_event_analysis = analyze_shape(wave_event,baseline_interval=baseline_interval,sample_points_window=sample_points_window,timestep=timestep,adc_to_mv=adc_to_mv,noise_threshold=noise_threshold,height_threshold=height_threshold)
    return wave_event_analysis

