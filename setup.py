import setuptools
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='rpyc_ext',  
    version='0.1',
    author="Demetrio Magatti",
    description="A simple test to make a structure for pip installation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ]
)
