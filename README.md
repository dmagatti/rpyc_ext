# rpyc_ext
This package is created as an extension of the rpyc package (https://gitlab.cern.ch/grigolet/rpyc) created by Gianluca Rigoletti to analyze data collected with RPC detectors. 
Several functions are hereby defined; generally they take as input the output files of Gianluca's script to manipulate the data and save them in a lite but full-of-information version.

# To do list
### add get_parameters_at-wp like function that 
<ul>
  <li>implements HV error using (p,T) variation during run</li>
  <li>(pre-step) implement fit function that uses also x-errors (HV errors)</li>
  <li>fits both efficiency and streamer curves</li>
  <li>checks fit goodness</li>
  <li>gets parameters using fit functions (if fit goodness is ok)</li>
</ul>

    
### add functions to somehow compute space-paralization and a time-paralization of the detector
<ul>
  <li>define space-paralization (charge threshold/strip) and time-paralization (pulse time)</li>
  <li>implement function that counts time duration of a pulse</li>
  <li>test it on standard gas mixture and on a suitable alternative, like He30+STD</li>
</ul>
